DROP TABLE if EXISTS "public"."base_entity";
CREATE TABLE "public"."base_entity" (
  "id" serial8 PRIMARY KEY,
  "name" varchar(60) COLLATE "pg_catalog"."default",
  "remark" json,
  created_user varchar(60) COLLATE "pg_catalog"."default",
  created_time timestamptz not null,
  updated_user varchar(60) COLLATE "pg_catalog"."default",
  updated_time timestamptz not null
);