package com.yukang.base.service;

import com.yukang.base.AbstractTest;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Date:     2020/12/8 4:38 下午<br/>
 *
 * @author yukang
 */
public class BaseServiceTest extends AbstractTest {

    @InjectMocks
    private BaseService baseService;

    @Test
    void testDoSomething() {
        // 设定参数
        String param = "param";
        // mock行为
        when(baseEntityRepo.findById(1L)).thenReturn(null);
        // 调用具体方法
        String result = baseService.doSomething(param);
        // 验证结果
        assertEquals(param,result);
    }

}