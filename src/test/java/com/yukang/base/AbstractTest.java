package com.yukang.base;

import com.yukang.base.dao.repo.BaseEntityRepo;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * Date:     2020/12/8 6:35 下午<br/>
 *
 * @author yukang
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractTest {

    @Mock
    protected BaseEntityRepo baseEntityRepo;

}
