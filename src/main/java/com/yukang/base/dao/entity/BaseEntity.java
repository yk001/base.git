package com.yukang.base.dao.entity;

import com.alibaba.fastjson.JSONObject;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Date:     2020/11/27 5:53 下午<br/>
 *
 * @author yukang
 */
@Data
@Entity
@Table(name="base_entity")
@TypeDef(name = "remarkType", typeClass = JsonBinaryType.class)
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity {

    @Id
    @SequenceGenerator(name="baseEntityIdSeq",sequenceName="base_entity_id_seq",allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="baseEntityIdSeq")
    private Long id;
    private String name;
    @Type(type = "remarkType")
    private JSONObject remark;
    @CreatedBy
    @Column(updatable = false)
    private String createdUser;
    @CreatedDate
    @Column(updatable = false)
    private Timestamp createdTime;
    @LastModifiedBy
    private String updatedUser;
    @LastModifiedDate
    private Timestamp updatedTime;

}
