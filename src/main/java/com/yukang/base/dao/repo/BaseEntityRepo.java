package com.yukang.base.dao.repo;

import com.yukang.base.dao.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Date:     2020/11/28 2:26 下午<br/>
 *
 * @author yukang
 */
public interface BaseEntityRepo extends JpaRepository<BaseEntity,Long>, QuerydslPredicateExecutor<BaseEntity> {

}
