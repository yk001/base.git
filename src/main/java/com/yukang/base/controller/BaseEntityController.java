package com.yukang.base.controller;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.yukang.base.dao.entity.BaseEntity;
import com.yukang.base.dao.entity.QBaseEntity;
import com.yukang.base.dao.repo.BaseEntityRepo;
import com.yukang.base.data.BaseEntityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
/**
 * Date:     2020/11/28 2:23 下午<br/>
 *
 * @author yukang
 */

@RestController
public class BaseEntityController {

    @Autowired
    private BaseEntityRepo baseEntityRepo;
    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @GetMapping("/baseentity/{id}")
    public Object getBaseEntity(@PathVariable("id") Long id) {
        return baseEntityRepo.findById(id).orElse(null);
    }

    @GetMapping("/baseentity")
    public Page<BaseEntity> getBaseEntitys(
            @RequestParam(name="createdTime",required = false) Timestamp createdTime,
            @RequestParam(name="page",defaultValue = "0") Integer page,
            @RequestParam(name="size",defaultValue = "10")  Integer size) {
        QBaseEntity qBaseEntity = QBaseEntity.baseEntity;
        BooleanBuilder booleanBuilder = new BooleanBuilder(qBaseEntity.id.isNotNull());
        if(createdTime != null){
            booleanBuilder.and(qBaseEntity.createdTime.goe(createdTime));
        }
        //分页排序
        Sort sort = Sort.by(Sort.Direction.ASC,"id");
        PageRequest pageRequest = PageRequest.of(page,size,sort);
        return baseEntityRepo.findAll(booleanBuilder.getValue(),pageRequest);
    }

    @GetMapping("/baseentityvo")
    public Page<BaseEntityVO> getBaseEntityVos(
            @RequestParam(name="createdTime",required = false) Timestamp createdTime,
            @RequestParam(name="page",defaultValue = "0") Integer page,
            @RequestParam(name="size",defaultValue = "10")  Integer size) {
        QBaseEntity qBaseEntity1 = QBaseEntity.baseEntity;
        QBaseEntity qBaseEntity2 = QBaseEntity.baseEntity;
        QBean<BaseEntityVO> qBean = Projections.bean(BaseEntityVO.class,
                qBaseEntity1.id,qBaseEntity1.name.as("name1"),qBaseEntity1.remark.as("remark1"),
                qBaseEntity2.name.as("name2"),qBaseEntity2.remark.as("remark2"));
        JPAQuery<BaseEntityVO> query = jpaQueryFactory.select(qBean)
                .from(qBaseEntity1)
                .leftJoin(qBaseEntity2).on(qBaseEntity1.id.eq(qBaseEntity2.id));
        query.orderBy(qBaseEntity1.id.asc()).offset(page*size).limit(size);
        if(createdTime != null){
            query.where(qBaseEntity1.createdTime.goe(createdTime));
        }
        QueryResults<BaseEntityVO> queryResults = query.fetchResults();
        PageRequest pageRequest = PageRequest.of(page,size);
        return new PageImpl<BaseEntityVO>(queryResults.getResults(),pageRequest,queryResults.getTotal());
    }

    @PostMapping("/baseentity")
    public Object saveBaseEntity(@RequestBody BaseEntity baseEntity) {
        return baseEntityRepo.save(baseEntity);
    }

    @PutMapping("/baseentity/{id}")
    public Object saveBaseEntity(@PathVariable("id") Long id, @RequestBody BaseEntity baseEntity) {
        baseEntity.setId(id);
        return baseEntityRepo.saveAndFlush(baseEntity);
    }

}
