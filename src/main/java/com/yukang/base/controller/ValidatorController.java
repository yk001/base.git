package com.yukang.base.controller;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;


/**
 * Function: Hibernate-Validator使用demo. <br/>
 * Date:     2020/12/13 3:29 下午<br/>
 *
 * @author yukang
 */
@RestController
@Validated
public class ValidatorController {

    @GetMapping("/entity")
    public String getEntity(
            @Max(10) @RequestParam(name="p1") Integer p1,
            @Max(10) @RequestParam(name="p2") Integer p2,
            @Max(10) @RequestParam(name="p3")  Integer p3) {
        return "success";
    }

}