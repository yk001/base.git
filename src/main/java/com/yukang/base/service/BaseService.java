package com.yukang.base.service;

import com.yukang.base.dao.repo.BaseEntityRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Date:     2020/12/8 4:34 下午<br/>
 *
 * @author yukang
 */
@Service
@Slf4j
public class BaseService {

    private final static Integer LOOP_COUNT = 10;
    @Autowired
    private BaseEntityRepo baseEntityRepo;

    public String doSomething(String param){
        baseEntityRepo.findById(1L);
        for(int i = 0;i<LOOP_COUNT;i++){
            log.info("i={}",i);
        }
        return param;
    }

}
