package com.yukang.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Date:     2020/12/8 4:34 下午<br/>
 *
 * @author yukang
 */
@SpringBootApplication
@EnableJpaAuditing
public class BaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseApplication.class, args);
	}

}
