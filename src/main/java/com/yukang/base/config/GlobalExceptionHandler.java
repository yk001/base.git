package com.yukang.base.config;

import com.yukang.base.common.CommonResultCode;
import com.yukang.base.common.GeneralResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;


/**
 * Function: GlobalExcept. <br/>
 * Date:     11/13/18 5:20 PM<br/>
 *
 * @see
 * @since JDK 1.8
 * @author yukang
 */
@Slf4j
@ControllerAdvice("com.yukang.base.controller")
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public GeneralResult otherExceptionHandler(HttpServletResponse response, Exception ex) {
        response.setStatus(500);
        GeneralResult tempResult = new GeneralResult();
        tempResult.setCode(CommonResultCode.OPERATION_FAILED_UNKOWN);
        tempResult.setDescription(ex.getMessage());
        return tempResult;
    }

}
