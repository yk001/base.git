package com.yukang.base.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * Date:     2020/11/30 5:27 下午<br/>
 *
 * @author yukang
 */
@Configuration
public class JpaAuditorConfig implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        // 可以根据SecurityContextHolder获取当前登录用户的信息
        return Optional.of("root");
    }

}
