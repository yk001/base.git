package com.yukang.base.data;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * Date:     2020/11/30 3:06 下午<br/>
 *
 * @author yukang
 */
@Data
public class BaseEntityVO {

    private Long id;
    private String name1;
    private String name2;
    private JSONObject remark1;
    private JSONObject remark2;

}
