package com.yukang.base.common;

/**
 * ClassName:ResultCode <br/>
 * Date: Jul 18, 2016 5:58:14 PM <br/>
 *
 * @author yukang
 * @since JDK 1.8
 * @see
 */
public interface CommonResultCode {
	/**OPERATION_SUCCESS*/
	Integer OPERATION_SUCCESS = 100;
	Integer OPERATION_FAILED_UNKOWN = 101;
	/** 参数校验失败*/
	Integer PARAM_ERROR = 400;
 	/** 请求资源不存在*/
	Integer SOURCE_NOT_FOUND = 404;
 	/** 操作被取消*/
	Integer OPERATION_CANCEL = 406;
 	/** 请求资源不足*/
	Integer SOURCE_NOT_ENOUGH = 409;
 	/** 预处理失败【获取锁失败】*/
	Integer PRECONDITION_FAILED = 412;
 	/** 未知执行异常*/
	Integer UN_KONWN_REEOR = 500;
 	/** 服务暂不可用*/
	Integer SERVICE_UNAVAILABLE = 503;

}
