/**
 * Project Name:liz-common-com.ioa.cloud.common.asm.utils
 * File Name:CommonConstant.java
 * Package Name:com.gemii.lizcloud.common.constant
 * Date:Sep 30, 20162:56:31 PM
 * Copyright (c) 2016, chenxj All Rights Reserved.
 */

package com.yukang.base.common;

/**
 * ClassName:CommonConstant <br/>
 * Date: Sep 30, 2016 2:56:31 PM <br/>
 *
 * @see
 * @since JDK 1.8
 * @author yukang
 */
public interface CommonConstant {

}
