package com.yukang.base.common;

import lombok.Data;

import java.io.Serializable;

/**
 * Function: 通用结果返回模型. <br/>
 * Date:     11/14/18 5:12 PM<br/>
 *
 * @see
 * @since JDK 1.8
 */
@Data
public class GeneralResult<T> implements Serializable {
    
	private static final long serialVersionUID = 1L;
	private Serializable code;
    private T data;
    private String description;

}
